
import { Fragment } from 'react'
import React, { useState, useEffect } from 'react';
import { Popover, Transition } from '@headlessui/react'
import { MenuIcon, XIcon } from '@heroicons/react/outline'


const navigation = [
  { name: 'Whitepaper', href: 'files/CryptoToday_Whitepaper.pdf' },
]

export default function App() {

  


    const calculateTimeLeft = () => {
        let year = new Date().getFullYear();
        const difference = +new Date(`03/07/${year}`) - +new Date();

        let timeLeft = {};

        if (difference > 0) {
            timeLeft = {
              DAYS: Math.floor(difference / (1000 * 60  * 60 * 24) | 0),
              HOURS: Math.floor((difference / (1000 * 60 * 60)) % 24 | 0),
              MINUTES: Math.floor((difference / 1000 / 60) % 60 | 0),
              SECONDS: Math.floor((difference / 1000) % 60 |  0)
            };
          }

          return timeLeft;
    };

    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

    useEffect(() => {

        const timer = setTimeout(() => {
          setTimeLeft(calculateTimeLeft());
        }, 1000);

        return () => clearTimeout(timer);

      });

      const timerComponents = [];

      Object.keys(timeLeft).forEach((interval) => {

        timerComponents.push(
            <div className="">

                <p className="mt-4">
                    {timeLeft[interval]}  
                </p>

                <p className="mt-2">
                    {interval}{" "}
                </p>

            </div>

        );
      });

      


  return (
    <div style={{fontFamily: 'Outfit' }} className="relative bg-white overflow-hidden">

      <div className="relative pt-6 pb-16 sm:pb-10">
        <Popover className="shadow-md pb-6">
          <div className="max-w-7xl mx-auto px-4 sm:px-6">
            <nav className="relative  flex items-center justify-between sm:h-10 md:justify-center" aria-label="Global">
              <div className="flex items-center flex-1 md:absolute md:inset-y-0 md:left-0">
                <div className="flex items-center justify-between w-full md:w-auto">
                  <a href="#">
                    <span className="sr-only">Crypto Today</span>
                    <img src='img/logo.svg' alt="Crypto today"  className="w-64"/>
                  </a>
                  <div className="-mr-2 flex items-center md:hidden">
                    <Popover.Button className="bg-gray-50 rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                      <span className="sr-only">Open main menu</span>
                      <MenuIcon className="h-6 w-6" aria-hidden="true" />
                    </Popover.Button>
                  </div>
                </div>
              </div>
              <div className="hidden md:flex md:space-x-10">

              </div>
              <div className="hidden md:absolute md:flex md:items-center md:justify-end md:inset-y-0 md:right-0">
                <span className="inline-flex rounded-md shadow">
                  <a href='files/CryptoToday_Whitepaper.pdf' style={{backgroundColor: '#056C9B'}} target="_blank" className=" transition ease-in-out duration-500 text-white px-6 py-3 font-bold rounded-lg">Whitepaper</a>
                </span>
              </div>
            </nav>
          </div>

          <Transition
            as={Fragment}
            enter="duration-150 ease-out"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="duration-100 ease-in"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <Popover.Panel
              focus
              className="absolute z-10 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
            >
              <div className="rounded-lg shadow-md bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
                <div className=" pt-4 flex items-center justify-between">
                  <div>
                  <img src="img/logo.svg" alt="Crypto today"  className="w-64"/>
                  </div>
                  <div className="-mr-2">
                    <Popover.Button className="bg-white rounded-md mr-4 p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                      <span className="sr-only">Close menu</span>
                      <XIcon className="h-6 w-6" aria-hidden="true" />
                    </Popover.Button>
                  </div>
                </div>
                <div className="px-2 py-6">
                  {navigation.map((item) => (
                    <a
                      key={item.name}
                      href={item.href}
                      target="_blank"
                      className="block px-3 py-2 rounded-md text-2xl font-medium text-gray-700 hover:text-gray-900 hover:bg-gray-50"
                    >
                      {item.name}
                    </a>
                  ))}
                </div>
              </div>
            </Popover.Panel>
          </Transition>
        </Popover>

        <main className="mt-6 px-4">
          <div className="text-center">
              <p style={{wordSpacing: "10px"}} className="mt-3 uppercase font-extrabold  mx-auto text-2xl text-center text-gray-800 sm:text-2xl md:mt-10  md:text-4xl ">
              Decentralized  crypto  research  platform  launches  in
              </p>
            <div className="mt-6 mx-auto grid grid-cols-2 md:flex md:flex-row gap-4 md:gap-8 sm:justify-center bg-gradient-to-l from-white via-gray-100  to-white text-red-600 rounded-xl text-3xl md:text-5xl font-extrabold py-6">
            {timerComponents.length ? timerComponents : <span>Time's up!</span>}
            </div>
          </div>
        </main>



        <div className="max-w-7xl mx-auto px-4">


          <p className="text-xl font-bold text-gray-900 text-center mt-10  md:mt-40">Join our socials & earn rewards!</p>

          <div className="flex items-center  flex-wrap gap-10 justify-center mt-8">

                <a className="hidden md:inline" href="https://discord.gg/cryptotodaycom" target="_blank"><img src="img/discord.svg" alt="" className="h-16"/></a>
                <a className="hidden md:inline" href="https://twitter.com/cryptotodaycom" target="_blank"><img src="img/twitter.svg" alt="" className="h-16"/></a>
                <a className="hidden md:inline" href="https://www.reddit.com/r/cryptotodaycom/" target="_blank"><img src="img/reddit.svg" alt="" className="h-16"/></a>
                <a className="hidden md:inline" href="https://github.com/cryptotodaycom" target="_blank"><img src="img/github.svg" alt="" className="h-16"/></a>
                <a className="hidden md:inline" href="https://cryptotodaycom.medium.com/" target="_blank"><img src="img/medium.svg" alt="" className="h-16"/></a>
                <a className="hidden md:inline" href="https://t.me/cryptotodaycom/24" target="_blank"><img src="img/telegram.svg" alt="" className="h-16"/></a>


                <a className="flex md:hidden" href="https://t.me/cryptotodaycom/24" target="_blank"><img src="img/telegram.svg" alt="" className="h-16"/></a>
                <a className="flex md:hidden" href="https://discord.gg/cryptotodaycom" target="_blank"><img src="img/discord.svg" alt="" className="h-16"/></a>
                <a className="flex md:hidden" href="https://twitter.com/cryptotodaycom" target="_blank"><img src="img/twitter.svg" alt="" className="h-16"/></a>

                <a className="flex md:hidden" href="https://www.reddit.com/r/cryptotodaycom/" target="_blank"><img src="img/reddit.svg" alt="" className="h-16"/></a>
                <a className="flex md:hidden" href="https://github.com/cryptotodaycom" target="_blank"><img src="img/github.svg" alt="" className="h-16"/></a>
                <a className="flex md:hidden" href="https://cryptotodaycom.medium.com/" target="_blank"><img src="img/medium.svg" alt="" className="h-16"/></a>




          </div>

          </div>

      </div>
    </div>
  )
}
